﻿namespace stack_tweaker
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonOpenFile = new System.Windows.Forms.Button();
            this.textBoxFileName = new System.Windows.Forms.TextBox();
            this.buttonStart = new System.Windows.Forms.Button();
            this.textBoxStatus = new System.Windows.Forms.TextBox();
            this.textBoxnewfanspeed = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxFileNameOut = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxFlowRate = new System.Windows.Forms.TextBox();
            this.checkBoxEnableFanSpeed = new System.Windows.Forms.CheckBox();
            this.checkBoxEnableFlowRate = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // buttonOpenFile
            // 
            this.buttonOpenFile.Location = new System.Drawing.Point(707, 19);
            this.buttonOpenFile.Name = "buttonOpenFile";
            this.buttonOpenFile.Size = new System.Drawing.Size(81, 25);
            this.buttonOpenFile.TabIndex = 0;
            this.buttonOpenFile.Text = "browse";
            this.buttonOpenFile.UseVisualStyleBackColor = true;
            this.buttonOpenFile.Click += new System.EventHandler(this.buttonOpenFile_Click);
            // 
            // textBoxFileName
            // 
            this.textBoxFileName.Location = new System.Drawing.Point(97, 22);
            this.textBoxFileName.Name = "textBoxFileName";
            this.textBoxFileName.Size = new System.Drawing.Size(604, 20);
            this.textBoxFileName.TabIndex = 1;
            // 
            // buttonStart
            // 
            this.buttonStart.Location = new System.Drawing.Point(691, 89);
            this.buttonStart.Name = "buttonStart";
            this.buttonStart.Size = new System.Drawing.Size(97, 44);
            this.buttonStart.TabIndex = 2;
            this.buttonStart.Text = "GO!";
            this.buttonStart.UseVisualStyleBackColor = true;
            this.buttonStart.Click += new System.EventHandler(this.buttonStart_Click);
            // 
            // textBoxStatus
            // 
            this.textBoxStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxStatus.Location = new System.Drawing.Point(12, 158);
            this.textBoxStatus.Multiline = true;
            this.textBoxStatus.Name = "textBoxStatus";
            this.textBoxStatus.Size = new System.Drawing.Size(776, 323);
            this.textBoxStatus.TabIndex = 3;
            // 
            // textBoxnewfanspeed
            // 
            this.textBoxnewfanspeed.Location = new System.Drawing.Point(183, 89);
            this.textBoxnewfanspeed.Name = "textBoxnewfanspeed";
            this.textBoxnewfanspeed.Size = new System.Drawing.Size(27, 20);
            this.textBoxnewfanspeed.TabIndex = 4;
            this.textBoxnewfanspeed.Text = "86";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 92);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(165, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "stack element 1st layer fan speed";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "GCODE file in";
            // 
            // textBoxFileNameOut
            // 
            this.textBoxFileNameOut.Location = new System.Drawing.Point(97, 48);
            this.textBoxFileNameOut.Name = "textBoxFileNameOut";
            this.textBoxFileNameOut.Size = new System.Drawing.Size(604, 20);
            this.textBoxFileNameOut.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 51);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "GCODE file out";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 116);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(158, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "stack element 1st layer flow rate";
            // 
            // textBoxFlowRate
            // 
            this.textBoxFlowRate.Location = new System.Drawing.Point(183, 113);
            this.textBoxFlowRate.Name = "textBoxFlowRate";
            this.textBoxFlowRate.Size = new System.Drawing.Size(27, 20);
            this.textBoxFlowRate.TabIndex = 9;
            this.textBoxFlowRate.Text = "97";
            // 
            // checkBoxEnableFanSpeed
            // 
            this.checkBoxEnableFanSpeed.AutoSize = true;
            this.checkBoxEnableFanSpeed.Checked = true;
            this.checkBoxEnableFanSpeed.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxEnableFanSpeed.Location = new System.Drawing.Point(217, 91);
            this.checkBoxEnableFanSpeed.Name = "checkBoxEnableFanSpeed";
            this.checkBoxEnableFanSpeed.Size = new System.Drawing.Size(112, 17);
            this.checkBoxEnableFanSpeed.TabIndex = 11;
            this.checkBoxEnableFanSpeed.Text = "enable adjustment";
            this.checkBoxEnableFanSpeed.UseVisualStyleBackColor = true;
            // 
            // checkBoxEnableFlowRate
            // 
            this.checkBoxEnableFlowRate.AutoSize = true;
            this.checkBoxEnableFlowRate.Checked = true;
            this.checkBoxEnableFlowRate.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxEnableFlowRate.Location = new System.Drawing.Point(217, 116);
            this.checkBoxEnableFlowRate.Name = "checkBoxEnableFlowRate";
            this.checkBoxEnableFlowRate.Size = new System.Drawing.Size(112, 17);
            this.checkBoxEnableFlowRate.TabIndex = 12;
            this.checkBoxEnableFlowRate.Text = "enable adjustment";
            this.checkBoxEnableFlowRate.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 493);
            this.Controls.Add(this.checkBoxEnableFlowRate);
            this.Controls.Add(this.checkBoxEnableFanSpeed);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.textBoxFlowRate);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBoxFileNameOut);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxnewfanspeed);
            this.Controls.Add(this.textBoxStatus);
            this.Controls.Add(this.buttonStart);
            this.Controls.Add(this.textBoxFileName);
            this.Controls.Add(this.buttonOpenFile);
            this.Name = "Form1";
            this.Text = "Stack Tweaker";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox textBoxFileName;
        public System.Windows.Forms.Button buttonOpenFile;
        public System.Windows.Forms.Button buttonStart;
        private System.Windows.Forms.TextBox textBoxStatus;
        private System.Windows.Forms.TextBox textBoxnewfanspeed;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxFileNameOut;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxFlowRate;
        private System.Windows.Forms.CheckBox checkBoxEnableFanSpeed;
        private System.Windows.Forms.CheckBox checkBoxEnableFlowRate;
    }
}

