﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace stack_tweaker
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void buttonOpenFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog fd = new OpenFileDialog();
            fd.ShowDialog();
            textBoxFileName.Text = fd.FileName;
            textBoxFileNameOut.Text = fd.FileName.Replace(".gcode", "_processed.gcode");
        }

        private void buttonStart_Click(object sender, EventArgs e)
        {

            System.IO.StreamReader file_in;
            System.IO.StreamWriter file_out;

            string last_fan_speed = "";
            UInt16 current_layer_number = 0;
            UInt16 last_layer_number = 0;
            bool start_of_new_model = false;

            float model_start_fan_speed = 4;
            UInt16 u_model_start_fan_speed = 4;

            uint u_model_start_flow_rate = 100;

            if (textBoxFileName.Text == "")
            {
                UpdateTextBox("Please select an input file");
                return;
            }


            if (!float.TryParse(textBoxnewfanspeed.Text, out model_start_fan_speed))
            {
                UpdateTextBox("please ensure fan speed is numeric");
                return;
            }

            if (model_start_fan_speed < 0 || model_start_fan_speed > 100)
            {
                UpdateTextBox("fan speed must be a percentage (0 - 100) ");
                return;
            }

            u_model_start_fan_speed = (UInt16)(model_start_fan_speed / 100 * 255);

            if (!uint.TryParse(textBoxFlowRate.Text, out u_model_start_flow_rate))
            {
                UpdateTextBox("please ensure flow rate is numeric");
                return;
            }


            file_in = new System.IO.StreamReader(textBoxFileName.Text);
            file_out = new System.IO.StreamWriter(textBoxFileNameOut.Text);

            while (!file_in.EndOfStream)
            {
                string current_line = file_in.ReadLine();
                file_out.WriteLine(current_line);


                if (current_line.Contains("M106"))
                {
                    last_fan_speed = current_line;
                    UpdateTextBox("fan speed found: " + last_fan_speed);
                }

                if (current_line.Contains(";LAYER:"))
                {
                    last_layer_number = current_layer_number;
                    current_layer_number = UInt16.Parse(current_line.Split(':')[1]);
                    //UpdateTextBox("layer: " + current_layer_number.ToString());

                    // do this check first to ensure correct detection
                    // of the end of the first layer of the new model
                    if (start_of_new_model == true)
                    {
                        UpdateTextBox("end of first layer for new model, layer: " + current_layer_number.ToString());
                        if (checkBoxEnableFanSpeed.Checked)
                        {
                            file_out.WriteLine(last_fan_speed);
                        }
                        if (checkBoxEnableFlowRate.Checked)
                        {
                            file_out.WriteLine("M221 S100");
                        }
                        start_of_new_model = false;
                    }

                    if (current_layer_number > last_layer_number + 1)
                    {
                        UpdateTextBox("start of next model in stack, layer: " + current_layer_number.ToString());
                        if (checkBoxEnableFanSpeed.Checked)
                        {
                            file_out.WriteLine("M106 S" + u_model_start_fan_speed);
                        }
                        if (checkBoxEnableFlowRate.Checked)
                        {
                            file_out.WriteLine("M221 S" + u_model_start_flow_rate);
                        }
                        start_of_new_model = true;
                    }
                }
            }
            file_in.Close();
            file_out.Close();
        }

        delegate void UpdateTextBoxCallback(string text);
        private void UpdateTextBox(string text)
        {
            if (this.textBoxStatus.InvokeRequired)
            {
                UpdateTextBoxCallback d = new UpdateTextBoxCallback(UpdateTextBox);
                this.Invoke(d, new object[] { text });
            }
            else
            {
                textBoxStatus.AppendText("\r\n" + DateTime.Now.ToString() + ": " + text);
            }
        }
    }




}
